﻿namespace EckertMensaBarcodeGenerator
{
    partial class CEckertMensaBarcodeGenerator
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CEckertMensaBarcodeGenerator));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu = new System.Windows.Forms.ToolStripMenuItem();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelSchuelernummer = new System.Windows.Forms.Label();
            this.txtEingabe = new System.Windows.Forms.TextBox();
            this.cmdCreate = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.cmdExit = new System.Windows.Forms.Button();
            this.imagebox = new System.Windows.Forms.PictureBox();
            this.groupboxbarcode = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagebox)).BeginInit();
            this.groupboxbarcode.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu,
            this.einstellungenToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(651, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu
            // 
            this.menu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.beendenToolStripMenuItem});
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(57, 24);
            this.menu.Text = "Datei";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(149, 26);
            this.dateiToolStripMenuItem.Text = "Speichern";
            this.dateiToolStripMenuItem.Click += new System.EventHandler(this.dateiToolStripMenuItem_Click);
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(149, 26);
            this.beendenToolStripMenuItem.Text = "Beenden";
            this.beendenToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.einstellungenToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // labelSchuelernummer
            // 
            this.labelSchuelernummer.AutoSize = true;
            this.labelSchuelernummer.Location = new System.Drawing.Point(13, 34);
            this.labelSchuelernummer.Name = "labelSchuelernummer";
            this.labelSchuelernummer.Size = new System.Drawing.Size(132, 20);
            this.labelSchuelernummer.TabIndex = 1;
            this.labelSchuelernummer.Text = "Schülernummer:";
            // 
            // txtEingabe
            // 
            this.txtEingabe.AcceptsTab = true;
            this.txtEingabe.Location = new System.Drawing.Point(151, 31);
            this.txtEingabe.Name = "txtEingabe";
            this.txtEingabe.Size = new System.Drawing.Size(104, 27);
            this.txtEingabe.TabIndex = 0;
            this.txtEingabe.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEingabe_KeyDown);
            // 
            // cmdCreate
            // 
            this.cmdCreate.Location = new System.Drawing.Point(17, 522);
            this.cmdCreate.Name = "cmdCreate";
            this.cmdCreate.Size = new System.Drawing.Size(109, 45);
            this.cmdCreate.TabIndex = 1;
            this.cmdCreate.Text = "Erzeugen";
            this.cmdCreate.UseVisualStyleBackColor = true;
            this.cmdCreate.Click += new System.EventHandler(this.cmdCreate_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Location = new System.Drawing.Point(146, 522);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(109, 45);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Speichern";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(528, 522);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(109, 45);
            this.cmdExit.TabIndex = 3;
            this.cmdExit.Text = "Beenden";
            this.cmdExit.UseVisualStyleBackColor = true;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // imagebox
            // 
            this.imagebox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imagebox.Location = new System.Drawing.Point(10, 40);
            this.imagebox.Name = "imagebox";
            this.imagebox.Size = new System.Drawing.Size(600, 400);
            this.imagebox.TabIndex = 4;
            this.imagebox.TabStop = false;
            // 
            // groupboxbarcode
            // 
            this.groupboxbarcode.Controls.Add(this.imagebox);
            this.groupboxbarcode.Location = new System.Drawing.Point(17, 70);
            this.groupboxbarcode.Name = "groupboxbarcode";
            this.groupboxbarcode.Size = new System.Drawing.Size(620, 435);
            this.groupboxbarcode.TabIndex = 5;
            this.groupboxbarcode.TabStop = false;
            this.groupboxbarcode.Text = "Barcode";
            // 
            // CEckertMensaBarcodeGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 576);
            this.Controls.Add(this.cmdExit);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.cmdCreate);
            this.Controls.Add(this.txtEingabe);
            this.Controls.Add(this.labelSchuelernummer);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupboxbarcode);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CEckertMensaBarcodeGenerator";
            this.Text = "EMBG - Eckert Mensa Barcode Generator";
            this.Load += new System.EventHandler(this.CEckertMensaBarcodeGenerator_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagebox)).EndInit();
            this.groupboxbarcode.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.Label labelSchuelernummer;
        private System.Windows.Forms.TextBox txtEingabe;
        private System.Windows.Forms.Button cmdCreate;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.Button cmdExit;
        private System.Windows.Forms.PictureBox imagebox;
        private System.Windows.Forms.GroupBox groupboxbarcode;
    }
}

