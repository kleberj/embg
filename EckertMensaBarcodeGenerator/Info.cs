﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Deployment.Application;
using System.Windows.Forms;

namespace EckertMensaBarcodeGenerator
{
    public partial class EMBGInfo : Form
    {
        public EMBGInfo()
        {
            InitializeComponent();
        }

        private void EMBGInfo_Load(object sender, EventArgs e)
        {
            // read data from AssemblyInfo.cs
            string copyright, version="N/A";
            Assembly asm = Assembly.GetExecutingAssembly();
            copyright = ((AssemblyCopyrightAttribute)asm.GetCustomAttribute(typeof(AssemblyCopyrightAttribute))).Copyright;
            // version is only available for deployed apps
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4);
            }
            lblversion.Text = version;
            lblcopyright.Text = copyright;
        }
    }
}
