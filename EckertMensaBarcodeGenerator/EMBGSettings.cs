﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BarcodeLib;

namespace EckertMensaBarcodeGenerator
{
    public partial class EMBGSettings : Form
    {
        // some default values
        private bool firstlabel = true;
        private Font settinglabelfont = new Font("Microsoft Sans Serif", 8);
        Color barcode = Color.Black;
        Color background = Color.White;

        // constructors
        public EMBGSettings()
        {
            InitializeComponent();
        }
        public EMBGSettings(string snummer)
        {
            InitializeComponent();
            txtLabel.Text = snummer;
        }

        // one set and a few get methods
        public string Label()
        {
            return txtLabel.Text.Trim();
        }

        public void Label(string label)
        {
            // set labelbox with student number at first call
            if(firstlabel)
                txtLabel.Text = label;
            firstlabel = false;
        }

        public Font LabelFont()
        {
            return settinglabelfont;
        }

        public bool IncludeLabel()
        {
            return cboxLabel.Checked;
        }

        public string LabelPosition()
        {
            return cbLabel.Text.Trim();
        }

        public Color ColorBarcode()
        {
            return barcode;
        }

        public Color ColorBackground()
        {
            return background;
        }

        public int BCwidth()
        {
            return Convert.ToInt32(nupBreite.Value);
        }

        public int BCheight()
        {
            return Convert.ToInt32(nupHoehe.Value);
        }
        
        // (un)set labelbox read only according to checkbox
        private void cboxLabel_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxLabel.Checked == true)
            {
                txtLabel.ReadOnly = false;
            }
            else
            {
                txtLabel.ReadOnly = true;
            }
        }

        private void cmdLabelFont_Click(object sender, EventArgs e)
        {
            // Font for exampletext with font size 10
            float fontsize=10;
            Font exampletextfont;

            if (fontDialogLabel.ShowDialog() == DialogResult.OK)
            {
                settinglabelfont = fontDialogLabel.Font;
            }
            else
                settinglabelfont = new Font("Microsoft Sans Serif", 8);
            // create new font as copy of settinglabelfont but font size 10
            exampletextfont = new Font(settinglabelfont.Name, fontsize, settinglabelfont.Style, settinglabelfont.Unit, settinglabelfont.GdiCharSet, settinglabelfont.GdiVerticalFont);
            fontexample.Font = exampletextfont;
        }

        // open colorDialog for bar code and background
        private void cmdColorBarcode_Click(object sender, EventArgs e)
        {
            if(colorDialogBarcode.ShowDialog()==DialogResult.OK)
            {
                barcode = colorDialogBarcode.Color;
                cmdColorBarcode.BackColor = barcode;
            }
        }

        private void cmdColorBackground_Click(object sender, EventArgs e)
        {
            if (colorDialogBackground.ShowDialog() == DialogResult.OK)
            {
                background = colorDialogBackground.Color;
                cmdColorBackground.BackColor = background;
            }
        }
    }
}
