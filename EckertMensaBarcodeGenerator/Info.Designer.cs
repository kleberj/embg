﻿namespace EckertMensaBarcodeGenerator
{
    partial class EMBGInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EMBGInfo));
            this.label1 = new System.Windows.Forms.Label();
            this.lblversion = new System.Windows.Forms.Label();
            this.cmdInfoOK = new System.Windows.Forms.Button();
            this.lblcopyright = new System.Windows.Forms.Label();
            this.lblLicense = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBarcodeLib = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 37);
            this.label1.MinimumSize = new System.Drawing.Size(600, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(600, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "Eckert Mensa Barcode Generator";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblversion
            // 
            this.lblversion.AutoSize = true;
            this.lblversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblversion.Location = new System.Drawing.Point(20, 86);
            this.lblversion.MinimumSize = new System.Drawing.Size(600, 0);
            this.lblversion.Name = "lblversion";
            this.lblversion.Size = new System.Drawing.Size(600, 29);
            this.lblversion.TabIndex = 1;
            this.lblversion.Text = "N/A";
            this.lblversion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdInfoOK
            // 
            this.cmdInfoOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdInfoOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdInfoOK.Location = new System.Drawing.Point(492, 380);
            this.cmdInfoOK.Name = "cmdInfoOK";
            this.cmdInfoOK.Size = new System.Drawing.Size(99, 46);
            this.cmdInfoOK.TabIndex = 2;
            this.cmdInfoOK.Text = "OK";
            this.cmdInfoOK.UseVisualStyleBackColor = true;
            // 
            // lblcopyright
            // 
            this.lblcopyright.AutoSize = true;
            this.lblcopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcopyright.Location = new System.Drawing.Point(20, 124);
            this.lblcopyright.MinimumSize = new System.Drawing.Size(600, 0);
            this.lblcopyright.Name = "lblcopyright";
            this.lblcopyright.Size = new System.Drawing.Size(600, 29);
            this.lblcopyright.TabIndex = 3;
            this.lblcopyright.Text = "(C) Josef Kleber 2018";
            this.lblcopyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLicense
            // 
            this.lblLicense.AutoSize = true;
            this.lblLicense.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicense.Location = new System.Drawing.Point(21, 167);
            this.lblLicense.MinimumSize = new System.Drawing.Size(600, 0);
            this.lblLicense.Name = "lblLicense";
            this.lblLicense.Size = new System.Drawing.Size(600, 29);
            this.lblLicense.TabIndex = 4;
            this.lblLicense.Text = "License: GPLv3";
            this.lblLicense.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 207);
            this.label2.MinimumSize = new System.Drawing.Size(600, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(600, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "https://kirza.kleberj.de/embg";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 282);
            this.label3.MinimumSize = new System.Drawing.Size(600, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(600, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Built with:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBarcodeLib
            // 
            this.lblBarcodeLib.AutoSize = true;
            this.lblBarcodeLib.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarcodeLib.Location = new System.Drawing.Point(24, 313);
            this.lblBarcodeLib.MinimumSize = new System.Drawing.Size(600, 0);
            this.lblBarcodeLib.Name = "lblBarcodeLib";
            this.lblBarcodeLib.Size = new System.Drawing.Size(600, 17);
            this.lblBarcodeLib.TabIndex = 7;
            this.lblBarcodeLib.Text = "barcodelib Copyright 2007-2018 Brad Barnhill (Apache License 2.0)";
            this.lblBarcodeLib.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EMBGInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 450);
            this.Controls.Add(this.lblBarcodeLib);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblLicense);
            this.Controls.Add(this.lblcopyright);
            this.Controls.Add(this.cmdInfoOK);
            this.Controls.Add(this.lblversion);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EMBGInfo";
            this.Text = "Info";
            this.Load += new System.EventHandler(this.EMBGInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblversion;
        private System.Windows.Forms.Button cmdInfoOK;
        private System.Windows.Forms.Label lblcopyright;
        private System.Windows.Forms.Label lblLicense;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblBarcodeLib;
    }
}