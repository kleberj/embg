﻿namespace EckertMensaBarcodeGenerator
{
    partial class EMBGSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EMBGSettings));
            this.cmdAbbrechen = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.grpboxLabel = new System.Windows.Forms.GroupBox();
            this.fontexample = new System.Windows.Forms.Label();
            this.cmdLabelFont = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbLabel = new System.Windows.Forms.ComboBox();
            this.txtLabel = new System.Windows.Forms.TextBox();
            this.cboxLabel = new System.Windows.Forms.CheckBox();
            this.fontDialogLabel = new System.Windows.Forms.FontDialog();
            this.grpboxColor = new System.Windows.Forms.GroupBox();
            this.cmdColorBackground = new System.Windows.Forms.Button();
            this.cmdColorBarcode = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.colorDialogBarcode = new System.Windows.Forms.ColorDialog();
            this.colorDialogBackground = new System.Windows.Forms.ColorDialog();
            this.grpboxDimensionen = new System.Windows.Forms.GroupBox();
            this.nupHoehe = new System.Windows.Forms.NumericUpDown();
            this.nupBreite = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grpboxLabel.SuspendLayout();
            this.grpboxColor.SuspendLayout();
            this.grpboxDimensionen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupHoehe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupBreite)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdAbbrechen
            // 
            this.cmdAbbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdAbbrechen.Location = new System.Drawing.Point(25, 314);
            this.cmdAbbrechen.Name = "cmdAbbrechen";
            this.cmdAbbrechen.Size = new System.Drawing.Size(137, 46);
            this.cmdAbbrechen.TabIndex = 7;
            this.cmdAbbrechen.Text = "Abbrechen";
            this.cmdAbbrechen.UseVisualStyleBackColor = true;
            // 
            // cmdOK
            // 
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOK.Location = new System.Drawing.Point(509, 314);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(137, 46);
            this.cmdOK.TabIndex = 8;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = true;
            // 
            // grpboxLabel
            // 
            this.grpboxLabel.Controls.Add(this.fontexample);
            this.grpboxLabel.Controls.Add(this.cmdLabelFont);
            this.grpboxLabel.Controls.Add(this.label1);
            this.grpboxLabel.Controls.Add(this.cbLabel);
            this.grpboxLabel.Controls.Add(this.txtLabel);
            this.grpboxLabel.Controls.Add(this.cboxLabel);
            this.grpboxLabel.Location = new System.Drawing.Point(25, 27);
            this.grpboxLabel.Name = "grpboxLabel";
            this.grpboxLabel.Size = new System.Drawing.Size(621, 111);
            this.grpboxLabel.TabIndex = 2;
            this.grpboxLabel.TabStop = false;
            this.grpboxLabel.Text = "Label";
            // 
            // fontexample
            // 
            this.fontexample.AutoSize = true;
            this.fontexample.Location = new System.Drawing.Point(476, 73);
            this.fontexample.Name = "fontexample";
            this.fontexample.Size = new System.Drawing.Size(96, 20);
            this.fontexample.TabIndex = 5;
            this.fontexample.Text = "Beispieltext";
            // 
            // cmdLabelFont
            // 
            this.cmdLabelFont.Location = new System.Drawing.Point(351, 69);
            this.cmdLabelFont.Name = "cmdLabelFont";
            this.cmdLabelFont.Size = new System.Drawing.Size(99, 28);
            this.cmdLabelFont.TabIndex = 2;
            this.cmdLabelFont.Text = "Schriftart";
            this.cmdLabelFont.UseVisualStyleBackColor = true;
            this.cmdLabelFont.Click += new System.EventHandler(this.cmdLabelFont_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Labelposition:";
            // 
            // cbLabel
            // 
            this.cbLabel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLabel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLabel.FormattingEnabled = true;
            this.cbLabel.Items.AddRange(new object[] {
            "Unten - zentriert",
            "Unten - rechts",
            "Unten - links",
            "Oben - zentriert",
            "Oben - rechts",
            "Oben - links"});
            this.cbLabel.Location = new System.Drawing.Point(171, 69);
            this.cbLabel.Name = "cbLabel";
            this.cbLabel.Size = new System.Drawing.Size(138, 28);
            this.cbLabel.TabIndex = 1;
            // 
            // txtLabel
            // 
            this.txtLabel.Location = new System.Drawing.Point(171, 25);
            this.txtLabel.Name = "txtLabel";
            this.txtLabel.ReadOnly = true;
            this.txtLabel.Size = new System.Drawing.Size(424, 27);
            this.txtLabel.TabIndex = 0;
            // 
            // cboxLabel
            // 
            this.cboxLabel.AutoSize = true;
            this.cboxLabel.Location = new System.Drawing.Point(16, 27);
            this.cboxLabel.Name = "cboxLabel";
            this.cboxLabel.Size = new System.Drawing.Size(158, 24);
            this.cboxLabel.TabIndex = 0;
            this.cboxLabel.TabStop = false;
            this.cboxLabel.Text = "Label hinzufügen";
            this.cboxLabel.UseVisualStyleBackColor = true;
            this.cboxLabel.CheckedChanged += new System.EventHandler(this.cboxLabel_CheckedChanged);
            // 
            // fontDialogLabel
            // 
            this.fontDialogLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.fontDialogLabel.FontMustExist = true;
            this.fontDialogLabel.MaxSize = 30;
            this.fontDialogLabel.MinSize = 8;
            this.fontDialogLabel.ShowApply = true;
            // 
            // grpboxColor
            // 
            this.grpboxColor.Controls.Add(this.cmdColorBackground);
            this.grpboxColor.Controls.Add(this.cmdColorBarcode);
            this.grpboxColor.Controls.Add(this.label3);
            this.grpboxColor.Controls.Add(this.label2);
            this.grpboxColor.Location = new System.Drawing.Point(25, 160);
            this.grpboxColor.Name = "grpboxColor";
            this.grpboxColor.Size = new System.Drawing.Size(397, 121);
            this.grpboxColor.TabIndex = 3;
            this.grpboxColor.TabStop = false;
            this.grpboxColor.Text = "Farben";
            // 
            // cmdColorBackground
            // 
            this.cmdColorBackground.BackColor = System.Drawing.Color.White;
            this.cmdColorBackground.Location = new System.Drawing.Point(142, 69);
            this.cmdColorBackground.Name = "cmdColorBackground";
            this.cmdColorBackground.Size = new System.Drawing.Size(99, 28);
            this.cmdColorBackground.TabIndex = 4;
            this.cmdColorBackground.UseVisualStyleBackColor = false;
            this.cmdColorBackground.Click += new System.EventHandler(this.cmdColorBackground_Click);
            // 
            // cmdColorBarcode
            // 
            this.cmdColorBarcode.BackColor = System.Drawing.Color.Black;
            this.cmdColorBarcode.Location = new System.Drawing.Point(142, 32);
            this.cmdColorBarcode.Name = "cmdColorBarcode";
            this.cmdColorBarcode.Size = new System.Drawing.Size(99, 28);
            this.cmdColorBarcode.TabIndex = 3;
            this.cmdColorBarcode.UseVisualStyleBackColor = false;
            this.cmdColorBarcode.Click += new System.EventHandler(this.cmdColorBarcode_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Hintergrund:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Barcode/Label:";
            // 
            // colorDialogBarcode
            // 
            this.colorDialogBarcode.AnyColor = true;
            // 
            // colorDialogBackground
            // 
            this.colorDialogBackground.AnyColor = true;
            // 
            // grpboxDimensionen
            // 
            this.grpboxDimensionen.Controls.Add(this.nupHoehe);
            this.grpboxDimensionen.Controls.Add(this.nupBreite);
            this.grpboxDimensionen.Controls.Add(this.label5);
            this.grpboxDimensionen.Controls.Add(this.label4);
            this.grpboxDimensionen.Location = new System.Drawing.Point(446, 160);
            this.grpboxDimensionen.Name = "grpboxDimensionen";
            this.grpboxDimensionen.Size = new System.Drawing.Size(200, 121);
            this.grpboxDimensionen.TabIndex = 4;
            this.grpboxDimensionen.TabStop = false;
            this.grpboxDimensionen.Text = "Barcode Dimensionen";
            // 
            // nupHoehe
            // 
            this.nupHoehe.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nupHoehe.Location = new System.Drawing.Point(88, 79);
            this.nupHoehe.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nupHoehe.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nupHoehe.Name = "nupHoehe";
            this.nupHoehe.Size = new System.Drawing.Size(72, 27);
            this.nupHoehe.TabIndex = 6;
            this.nupHoehe.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // nupBreite
            // 
            this.nupBreite.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nupBreite.Location = new System.Drawing.Point(88, 41);
            this.nupBreite.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.nupBreite.Minimum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.nupBreite.Name = "nupBreite";
            this.nupBreite.Size = new System.Drawing.Size(72, 27);
            this.nupBreite.TabIndex = 5;
            this.nupBreite.Value = new decimal(new int[] {
            450,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Höhe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Breite";
            // 
            // EMBGSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 387);
            this.Controls.Add(this.grpboxDimensionen);
            this.Controls.Add(this.grpboxColor);
            this.Controls.Add(this.grpboxLabel);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdAbbrechen);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EMBGSettings";
            this.Text = "Einstellungen";
            this.grpboxLabel.ResumeLayout(false);
            this.grpboxLabel.PerformLayout();
            this.grpboxColor.ResumeLayout(false);
            this.grpboxColor.PerformLayout();
            this.grpboxDimensionen.ResumeLayout(false);
            this.grpboxDimensionen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupHoehe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupBreite)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmdAbbrechen;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.GroupBox grpboxLabel;
        private System.Windows.Forms.Button cmdLabelFont;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbLabel;
        private System.Windows.Forms.TextBox txtLabel;
        private System.Windows.Forms.CheckBox cboxLabel;
        private System.Windows.Forms.FontDialog fontDialogLabel;
        private System.Windows.Forms.GroupBox grpboxColor;
        private System.Windows.Forms.Button cmdColorBackground;
        private System.Windows.Forms.Button cmdColorBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColorDialog colorDialogBarcode;
        private System.Windows.Forms.ColorDialog colorDialogBackground;
        private System.Windows.Forms.Label fontexample;
        private System.Windows.Forms.GroupBox grpboxDimensionen;
        private System.Windows.Forms.NumericUpDown nupBreite;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nupHoehe;
    }
}