﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BarcodeLib;

namespace EckertMensaBarcodeGenerator
{
    public partial class CEckertMensaBarcodeGenerator : Form
    {
        // default values
        private Barcode barcode = new Barcode();
        private int height = 300;
        private int width = 450;
        private string label = "";
        private bool includelabel = false;
        private LabelPositions labelposition = LabelPositions.TOPCENTER;
        private bool image_created = false;
        private string schuelernummer = "";
        private Font labelfont = new Font("Microsoft Sans Serif", 8);
        private Color color_barcode = Color.Black;
        private Color color_background = Color.White;
        // create new infoDialog object
        private EMBGInfo infoDialog = new EMBGInfo();
        // create new settingsDialog object
        private EMBGSettings settingsDialog = new EMBGSettings();

        public CEckertMensaBarcodeGenerator()
        {
            InitializeComponent();
        }

        // Exit button
        private void cmdExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // initialize some vars onLoad 
        private void CEckertMensaBarcodeGenerator_Load(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(1, 1);
            bmp.SetPixel(0, 0, Color.White);
            imagebox.Image = (Image)bmp;
            // make imagebox invisible at start
            imagebox.Visible = false;
        }

        // create barcode
        private void cmdCreate_Click(object sender, EventArgs e)
        {
            int checknumber = 0;

            // read schuelernummer from txtEingabe
            schuelernummer = txtEingabe.Text.Trim();
            // set label in settingsDialog
            settingsDialog.Label(schuelernummer);

            if (schuelernummer != "")
            {
                try
                {
                    checknumber = Convert.ToInt32(schuelernummer);
                    // is input a realistic student number
                    if (checknumber > 99999999 || checknumber < 0)
                    {
                        MessageBox.Show("Das ist nicht deine Schülernummer! ;-)");
                        txtEingabe.Clear();
                        txtEingabe.Focus();
                    }
                    else
                    {
                        // create barcode with current settings
                        barcode.IncludeLabel = includelabel;
                        barcode.AlternateLabel = label;
                        barcode.LabelFont = labelfont;
                        barcode.LabelPosition = labelposition;
                        barcode.Height = height;
                        barcode.Width = width;
                        barcode.ForeColor = color_barcode;
                        barcode.BackColor = color_background;
                        imagebox.Image = barcode.Encode(TYPE.CODE93, schuelernummer);
                        imagebox.Height = imagebox.Image.Height;
                        imagebox.Width = imagebox.Image.Width;
                        // center barcode inside groupbox
                        imagebox.Location = new Point((groupboxbarcode.Width / 2) - barcode.Width / 2,
                                                      (groupboxbarcode.Height / 2) - barcode.Height / 2);
                        // make imagebox visible
                        imagebox.Visible = true;
                        // barcode created?? done!!
                        image_created = true;
                    }
                }
                // catch non number input
                catch(FormatException)
                {
                    MessageBox.Show("Gib bitte nur Zahlen als Schülernummer ein!");
                    txtEingabe.Clear();
                    txtEingabe.Focus();
                }
                // show error message in case of an error
                catch (Exception ex)
                {
                    MessageBox.Show("Allgemeiner Systemfehler: " + ex.ToString());
                }
            }
            // txtEingabe emtpy
            else
                MessageBox.Show("Bitte gib deine Schülernummer ein!");
        }

        // if 'Enter' pressed in txtEingabe -> execute cmdCreate_Click
        // mapped txtEingabe's Keydown event with txtEingabe_Keydown
        private void txtEingabe_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cmdCreate_Click(this, new EventArgs());
            }
        }

        // Menu 'Beenden' -> cmdExit_Click
        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cmdExit_Click(this, new EventArgs());
        }

        // open settings form
        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // set setting label with with current student number
            // only first time!!
            settingsDialog.Label(txtEingabe.Text.Trim());

            if (settingsDialog.ShowDialog() == DialogResult.OK)
            {
                string lpos;

                // if DialogResult.OK -> move settings into vars
                includelabel = settingsDialog.IncludeLabel();
                label = settingsDialog.Label();
                lpos = settingsDialog.LabelPosition();
                switch (lpos)
                {
                    case "Unten - zentriert": labelposition = LabelPositions.BOTTOMCENTER; break;
                    case "Unten - rechts": labelposition = LabelPositions.BOTTOMRIGHT; break;
                    case "Unten - links": labelposition = LabelPositions.BOTTOMLEFT; break;
                    case "Oben - zentriert": labelposition = LabelPositions.TOPCENTER; break;
                    case "Oben - rechts": labelposition = LabelPositions.TOPRIGHT; break;
                    case "Oben - links": labelposition = LabelPositions.TOPLEFT; break;
                    default:
                        labelposition = LabelPositions.TOPCENTER;
                        break;
                }
                labelfont = settingsDialog.LabelFont();
                color_barcode = settingsDialog.ColorBarcode();
                color_background = settingsDialog.ColorBackground();
                width = settingsDialog.BCwidth();
                height = settingsDialog.BCheight();
            }
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // open infoDialog and do nothing until 'OK'
            if (infoDialog.ShowDialog() == DialogResult.OK)
            {
                // do nothing; show info form
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (image_created == true)
            {
                // create new SaveFileDialog
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.FileName = "barcode";
                // set file filter to png|jpg|gif|bmp|tif
                sfd.Filter = "PNG (*.png)|*.png|JPG (*.jpg)|*.jpg|GIF (*.gif)|*.gif|BMP (*.bmp)|*.bmp|TIFF (*.tif)|*.tif";
                // add file extension if not specified
                sfd.AddExtension = true;
                // show SaveFileDialog and save file
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    SaveTypes savetype = SaveTypes.UNSPECIFIED;
                    switch (sfd.FilterIndex)
                    {
                        case 1: savetype = SaveTypes.PNG; break;
                        case 2: savetype = SaveTypes.JPG; break;
                        case 3: savetype = SaveTypes.GIF; break;
                        case 4: savetype = SaveTypes.BMP; break;
                        case 5: savetype = SaveTypes.TIFF; break;
                        default: break;
                    }
                    barcode.SaveImage(sfd.FileName, savetype);
                }
            }
            else
                MessageBox.Show("Erzeuge bitte erst einen Barcode! ;-)");
        }

        // Menu 'Speichern' -> cmdSave_Click
        private void dateiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cmdSave_Click(this, new EventArgs());
        }
    }
}
